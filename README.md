# Checkass

## Introduction

This is a simple full stack [React](https://reactjs.org/) application with a [Node.js](https://nodejs.org/en/) and [Express](https://expressjs.com/) backend. Client side code is written in React and the backend API is written using Express. This application is configured with [Airbnb's ESLint rules](https://github.com/airbnb/javascript) and formatted through [prettier](https://prettier.io/).

### Development mode

In the development mode, we will have 2 servers running. The front end code will be served by the [webpack dev server](https://webpack.js.org/configuration/dev-server/) which helps with hot and live reloading. The server side Express code will be served by a node server using [nodemon](https://nodemon.io/) which helps in automatically restarting the server whenever server side code changes.

### Production mode

In the production mode, we will have only 1 server running. All the client side code will be bundled into static files using webpack and it will be served by the Node.js/Express application.

## Quick Start

```bash
# Clone the repository
git clone ......... checkass

# Go inside the directory
cd checkass

# Install dependencies
yarn (or npm install)

# Start development server
yarn dev (or npm run dev)

# Build for production
yarn build (or npm run build)

# Start production server
yarn start (or npm start)
```

## Documentation

### Folder Structure

All the source code will be inside **src** directory. Inside src, there is client and server directory. All the frontend code (react, css, js and any other assets) will be in client directory. Backend Node.js/Express code will be in the server directory.

### Webpack dev server

[Webpack dev server](https://webpack.js.org/configuration/dev-server/) is used along with webpack. It provides a development server that provides live reloading for the client side code. This should be used for development only.

[**Port**](https://webpack.js.org/configuration/dev-server/#devserver-port) specifies the Webpack dev server to listen on this particular port (3000 in this case). When [**open**](https://webpack.js.org/configuration/dev-server/#devserver-open) is set to true, it will automatically open the home page on startup. [Proxying](https://webpack.js.org/configuration/dev-server/#devserver-proxy) URLs can be useful when we have a separate API backend development server and we want to send API requests on the same domain. In our case, we have a Node.js/Express backend where we want to send the API requests to.

### Nodemon

Nodemon is a utility that will monitor for any changes in the server source code and it automatically restart the server. This is used in development only.

nodemon.json file is used to describe the configurations for Nodemon. Below is the nodemon.json file which I am using.

```javascript
{
  "watch": ["src/server/"]
}
```

Here, we tell nodemon to watch the files in the directory src/server where out server side code resides. Nodemon will restart the node server whenever a file under src/server directory is modified.

### Concurrently

[Concurrently](https://github.com/kimmobrunfeldt/concurrently) is used to run multiple commands concurrently. It is used to run the webpack dev server and the backend node server concurrently in the development environment.

### VSCode + ESLint + Prettier

[VSCode](https://code.visualstudio.com/) is a lightweight but powerful source code editor. [ESLint](https://eslint.org/) takes care of the code-quality. [Prettier](https://prettier.io/) takes care of all the formatting.

#### Installation guide

1.  Install [VSCode](https://code.visualstudio.com/)
2.  Install [ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
3.  Install [Prettier extension](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
4.  Modify the VSCode user settings to add below configuration

    ```javascript
    "eslint.alwaysShowStatus": true,
    "eslint.autoFixOnSave": true,
    "editor.formatOnSave": true,
    "prettier.eslintIntegration": true
    ```


api/fetch-categories
      [
          {
              "_id": "5d8dd3e88ca17342f4f460f5",
              "title": "Some new catigory",
              "__v": 0
          },
          {
              "_id": "5d8dde69b0b7fe49bb2c4990",
              "title": "Some catigory",
              "__v": 0
          }
      ] - respons


   api/fetch-actors
    [
    {
        "_id": "5d8dbf9e8684fc3664d6b0f3",
        "name": "actor1",
        "__v": 0
    },] - response

api/fetch-videos
  request data: { search: 'vide', //text searct
                  category: '5d8cc418d153c012020b2194', // _id catygory 
                  actors: [ 'actor2', 'actor1' ] } // array actors
  [
    {
        "actors": [
            "actor1",
            "actor2"
        ],
        "_id": "5d8cb0c454be5c05cefae623",
        "tegs": [
            "tag1",
            "tag2",
            "tag3",
            "tag3"
        ],
        "title": "video",
        "shortDescription": "Lorem ipsum dolor sit amet consectetur adipisicing elit",
        "fullDescription": "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint aspernatur unde magnam pariatur odit. Ducimus eos provident velit tempore eum! Earum atque illum mollitia voluptas reprehenderit corporis, ipsum minima enim?",
        "quality": "some quality",
        "imageUrl": "https://s3.eu-central-1.wasabisys.com/screanvideo/video.png",
        "publishDate": "2019-09-26T14:26:26.361Z",
        "category": "5d8cc418d153c012020b2194",
        "url": "https://s3.eu-central-1.wasabisys.com/testbaket/video.mp4"
    },] - respons


