import {
    LOADING_VIDEOS,
    FETCH_ALL_VIDEOS,
    ADD_FILTER,
    ADD_SEARCH,
    ADD_MULTISEARCH,
    FETCH_FILTERED_VIDEOS
} from './types';

export const videosIsIsntLoading = (isLoading) => ({
    type: LOADING_VIDEOS,
    isLoading: isLoading
});

export const fetchAllVideos = (videos) => ({
    type: FETCH_ALL_VIDEOS,
    isLoading: false,
    videos: videos
});

export const fetchFilteredVideos = (videos) => ({
    type: FETCH_FILTERED_VIDEOS,
    isLoading: false,
    videos: videos
});

export const addFilters = (filters) => ({
    type: ADD_FILTER,
    filters: { ...filters }
});

export const addSearch = (search) => ({
    type: ADD_SEARCH,
    search
});

export const addMultiSearch = (multiSearch) => ({
    type: ADD_MULTISEARCH,
    multiSearch
});