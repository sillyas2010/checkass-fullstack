import * as actions from '../actions/types';

const defaultVideosState = {
    all : [],
    filtered: [],
    multiSearch: [],
    filters: {},
    search: '',
    isLoading: false
};

export default function videosReducer(state = defaultVideosState, action) {
    switch (action.type) {
        case actions.LOADING_VIDEOS:
                return { ...state, isLoading: action.isLoading };
        case actions.FETCH_ALL_VIDEOS:
            return { ...state, all: [ ...action.videos ], isLoading: action.isLoading };
        case actions.ADD_FILTER:
            return { ...state, filters: { ...action.filters } };
        case actions.ADD_SEARCH:
                return { ...state, search: action.search };
        case actions.ADD_MULTISEARCH:
                return { ...state, multiSearch: [ ...action.multiSearch ] };
        case actions.FETCH_FILTERED_VIDEOS:
            return { ...state, filtered: [ ...action.videos ], isLoading: action.isLoading };
        default:
            return state;
    }
}