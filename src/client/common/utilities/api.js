import axios from 'axios';

import config from './config';

export const fetchVideosPromise = (resolve, reject, filters) => {
    axios.get(`${config.API_ENDPOINT}/fetch-videos`, { params: { filters } })
    .then((res) => {
        if(resolve) {
            resolve(res);
        }
    })
    .catch((err) => {
        if(reject) {
            reject(err);
        }
    })
}

export const fetchFiltersPromise = (resolve, reject, filters) => {
    axios.get(`${config.API_ENDPOINT}/fetch-filters`)
    .then((res) => {
        if(resolve) {
            resolve(res);
        }
    })
    .catch((err) => {
        if(reject) {
            reject(err);
        }
    })
}