import React, { Component } from 'react';
import { Switch } from 'react-router-dom';

import PropsRoute from './PropsRoute.js';
import Home from '../components/Home/Home';
import VideoDetail from '../components/VideoDetail/VideoDetail';

export default
class Routes extends Component {
    render() {
        let { setRouteTitle } = this.props;

        return (
            <Switch>
                <PropsRoute onEnter={() => setRouteTitle('Home')} key={0} exact path="/" component={Home}/>
                <PropsRoute onEnter={() => setRouteTitle('Detail')} key={1} path="/videos/:videoId" component={VideoDetail}/>
            </Switch>
        )
    }
}