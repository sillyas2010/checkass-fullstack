import React, {PureComponent} from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { toast } from 'react-toastify';
import { Player } from 'video-react';

import { fetchVideosPromise } from './../../common/utilities/api';
import { fetchAllVideos, videosIsIsntLoading } from './../../actions';

import Footer from '../Footer/Footer';

import './videoDetail.scss';

class VideoDetail extends PureComponent {
    state = {
        currentVideo: {},
        loading: true,
        paused: true,
        notFound: false,
        hasStarted: false,
        similarVideos: [],
        similarVisible: false,
        duration: '-:--'
    }

    componentDidMount() {
        if(!this.props.videos.length) {
            this.props.videosIsIsntLoading(true);
            fetchVideosPromise((res) => {
                if(res && res.data) {
                    const videosArr = [ ...res.data ];
                    this.findCurrentVideo(videosArr);
                    this.props.fetchAllVideos(videosArr);
                }
            }, (err) => {
                this.findCurrentVideo([]);
                this.props.videosIsIsntLoading(false);
                toast.error('Error!');
            })
        } else {
            this.findCurrentVideo(this.props.videos);
        }
    }

    componentWillUnmount() {
        if(this.player) {
            this.player.video.video.removeEventListener('play', this.onVideoStatusChange, false);
            this.player.video.video.removeEventListener('pause', this.onVideoStatusChange, false);
        }

        if(this.posterWrapper) {
            this.posterWrapper.removeEventListener("mouseover", this.hideShowSimilar, false);
            this.posterWrapper.removeEventListener("mouseleave", this.hideShowSimilar, false);
        }
    }

    fancyDurationFormat = (time) => {
        let seconds = Math.floor(time),
            // Hours, minutes and seconds
            hrs = ~~(seconds / 3600),
            mins = ~~((seconds % 3600) / 60),
            secs = ~~seconds % 60,
            // Output like "1:01" or "4:03:59" or "123:03:59"
            ret = "";

        if (hrs > 0) {
            ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
        }

        ret += "" + mins + ":" + (secs < 10 ? "0" : "");
        ret += "" + secs;
        return ret;
    }

    findCurrentVideo(videosArr) {
        let currentHash = this.props.history ? this.props.history.location.pathname : window.location.hash,
            currentVideoId = (currentHash.split('/')).pop(),
            oldVideo = this.state.currentVideo,
            currentVideo = videosArr.find((video) => video._id === currentVideoId);

        if(Object.keys(oldVideo).length) {
            if(oldVideo._id === currentVideoId) {
                return;
            }
        }

        if(currentVideo) {
            this.setState({
                currentVideo
            });

            this.getSimilarVideos(currentVideo);
        } else {
            this.setState({
                notFound: true,
                loading: false
            })
        }
    }

    getSimilarVideos(currentVideo) {
        let currentCategory = currentVideo.category && currentVideo.category._id ? currentVideo.category._id : '';

        if(currentCategory) {
            let newFilters = {
                category: currentCategory
            };
            
            fetchVideosPromise(
                (res) => {
                    if(res.data && res.data.length) {
                        let similarVideos = res.data.filter(video => video._id !== currentVideo._id);

                        if(similarVideos.length) {
                            this.setState({
                                similarVideos
                            });

                            if(this.posterWrapper) {
                                this.posterWrapper.addEventListener("mouseover", this.hideShowSimilar, false);
                                this.posterWrapper.addEventListener("mouseleave", this.hideShowSimilar, false);
                            }
                        }
                    }
                },
                (err) => {
                    toast.error('Error!');
                }, 
                newFilters
            );
        }
    }

    hideShowSimilar = (e) => {
        if(!this.state.hasStarted) {
            let newState = {};

            if(e.type === 'mouseover') {
                newState.similarVisible = true;
            } else {
                newState.similarVisible = false;
            }
            
            this.setState(newState);
        }
    }

    setPlayerRef = (player) => {
        this.player = player;
        if(this.player) {
            let oldFunction = this.player.video.handleLoadedMetaData;

            this.player.video.handleLoadedMetaData = (e) => {
                oldFunction(e);
                this.handleDuration(e);
            }

            this.player.video.video.addEventListener('play', this.onVideoStatusChange, false);
            this.player.video.video.addEventListener('pause', this.onVideoStatusChange, false);
        }
    }

    setScrollRef = (similarList) => {
        this.similarList = similarList;
    }

    setPosterWrapperRef = (posterWrapper) => {
        this.posterWrapper = posterWrapper;
    }

    handleDuration = (e) => {
        if(e.target && e.target.duration) {
            let duration = this.fancyDurationFormat(e.target.duration);

            this.setState({
                duration,
                loading: false
            });
        } else {
            this.setState({
                loading: false
            });
        }
    }

    playTheVideo = () => {
        if(this.player) {
            if(!this.state.hasStarted) {
                this.setState({
                    hasStarted: true,
                    similarVisible: false
                })
            }
            this.player.play();
        }
    }

    onVideoStatusChange = (e) => {
        let newState = {};

        if (e.type == 'play') {
            newState.paused = false;
        } else if (e.type == 'pause') {
            newState.paused = true;
        }

        this.setState(newState);
    }

    componentDidUpdate() {
        let videosArr = this.props.videos;

        if(videosArr) {
            this.findCurrentVideo(videosArr);
        }
    }

    onListScroll = (direction) => {
        if(this.similarList) {
            let length = 0,
                scrollObj = {
                    behavior: 'smooth'
                };

            if(this.similarList.clientWidth) {
                length = this.similarList.clientWidth;

                if(direction == 'right') {
                    length = -Math.abs(length);
                }

                length = this.similarList.scrollLeft + length;
            }
                
            scrollObj.left = length;
        
            this.similarList.scrollTo(scrollObj);
        }
    }

    render() {
        let { title, shortDescription, fullDescription, quality, actors, imageUrl, url } = this.state.currentVideo,
            goBack = this.props.history ? () => { this.props.history.goBack() } : () => { window.history.back() };

        return (
            <>
                <main className={classNames("video-detail-main", { 'loading active': this.props.isLoading })}>
                    <section className={classNames("video-detail-poster-wrapper", { 'loading active': this.state.loading })} ref={this.setPosterWrapperRef}>
                        <div className={classNames("video-wrapper", { 'paused': this.state.paused })}>
                            <Player
                                poster={imageUrl}
                                src={url}
                                ref={this.setPlayerRef}
                            />
                        </div>
                        <div className={classNames("short-info-block", { 'hidden': this.state.hasStarted })}>
                            <div className="container">
                                <div className="info-text">
                                    <h2 className="info-title">{title}</h2>
                                    <span className="info-description">{shortDescription}</span>
                                </div>
                            </div>
                        </div>
                        <div className={classNames("actions-block", { 'hidden': !this.state.paused })}>
                            <div className="play-video-button" onClick={this.playTheVideo}>
                                <div className="play-button"></div>
                                <span className="play-text">Watch the video</span>
                            </div>
                            <div className="arrow-button back-button" onClick={goBack}></div>
                        </div>
                        { this.state.similarVideos.length ?
                            <section className={classNames("similar-videos-wrapper", { 'active': this.state.similarVisible })}>
                                <div className="relative-container">
                                    <div className="list-control-wrapper">
                                        <div className="arrow-button back-button" onClick={() => {this.onListScroll('right')}}></div>
                                        <div className="arrow-button next-button" onClick={() => {this.onListScroll('left')}}></div>
                                    </div>
                                    <div className="container">
                                        <div className="videos-list-wrapper">
                                            <div className="similar-videos-list">
                                                <div className="row videos-row" ref={this.setScrollRef}>
                                                    {
                                                        this.state.similarVideos.map((video) => (
                                                            <article key={(video._id).toString()} className="video-col col-12 col-sm-6 col-md-4 col-lg-3">
                                                                <div className="video-wrapper similar-video-wrapper">
                                                                    <Link to={`/videos/${video._id}`}>
                                                                        <div className="video-poster-wrapper" 
                                                                            style={{ backgroundImage: (video.imageUrl ? `url(${video.imageUrl})` : undefined) }}
                                                                        ></div>
                                                                    </Link>
                                                                </div>
                                                            </article>
                                                        ))
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            :
                            ''
                        }
                    </section>
                    <section className="video-detail-info-wrapper">
                        <div className="container">
                            <div className="row">
                                <div className="col-9 col-md-3">
                                    <div className="info-section info-full-description">
                                        <h3 className="info-heading">
                                            Description
                                        </h3>
                                        <span className="info-content">
                                            {fullDescription}
                                        </span>
                                    </div>
                                </div>
                                <div className="col-3 col-md-2">
                                    <div className="info-section info-full-duration">
                                        <h3 className="info-heading">
                                            Duration
                                        </h3>
                                        <span className="info-content big">
                                            {this.state.duration}
                                        </span>
                                    </div>
                                </div>
                                <div className="col-6 col-md-2">
                                    <div className="info-section info-quality">
                                        <h3 className="info-heading">
                                            Quality
                                        </h3>
                                        <span className="info-content big">
                                            {quality}
                                        </span>
                                    </div>
                                </div>
                                <div className="col-6 col-md-5">
                                    <div className="info-section info-actors">
                                        <h3 className="info-heading">
                                            Performer
                                        </h3>
                                        <span className="info-content big actors">
                                            { actors && actors.length &&
                                                actors.map((actor, index) => (
                                                    `${actor.title}${ (actors.length - 1) !== index ? ', ' : '' }`
                                                ))
                                            }
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
                <Footer onlyMobile={true} isFull={true}/>
            </>
        );
    }
}

const mapStoreToProps = store => {
    let { all, filtered, filters, search, isLoading } = store.videosReducer;

    return {
        videos: [ ...all ],
        filtered: [ ...filtered ],
        filters: { ... filters },
        search,
        isLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchAllVideos,
        videosIsIsntLoading
    }, dispatch)
};

export default connect(mapStoreToProps, mapDispatchToProps)(withRouter(VideoDetail));