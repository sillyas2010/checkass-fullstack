import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { toast } from 'react-toastify';

import { fetchFilteredVideos, addFilters, videosIsIsntLoading } from './../../actions';
import { fetchFiltersPromise, fetchVideosPromise } from './../../common/utilities/api';

import './filters.scss';

class Filters extends Component {
    state = {
        loading: true,
        filtersList: [],
        currentFilters: {}
    }

    componentDidMount() {
        fetchFiltersPromise((res) => {
            if(res && res.data) {
                let currentFilters = {};

                res.data.forEach((filter) => {
                    currentFilters[filter.attr] = '-1';
                })

                this.setState({
                    filtersList: [ ...res.data ],
                    currentFilters,
                    loading: false
                });
            }
        }, (err) => {
            this.setState({
                loading: false
            });
            toast.error('Error!');
        });
    }

    filterChanged(e, attr, index) {
        let { value } = e.target,
            oldFilters = { ...this.props.activeFilters },
            newFilters = { ...this.props.activeFilters },
            currentFilter = this.state.filtersList[index];

        if(attr) {
            if(value === '-1') {
                delete newFilters[attr];
            } else {
                newFilters[attr] = currentFilter.list[value]._id || '';
            }

            this.props.addFilters(newFilters);

            if(this.props.search) {
                newFilters.search = this.props.search;
            }

            this.props.videosIsIsntLoading(true);
            fetchVideosPromise(
             (res) => {
                this.props.fetchFilteredVideos(res.data);
             },
             (err) => {
                toast.error('Error!');
                this.props.addFilters(oldFilters);
                this.props.videosIsIsntLoading(false);
             }, 
             newFilters
            );
        }
    }

    render() {
        return (
            <div className={classNames("filters-list-wrapper", { 'loading active': (this.state.loading && this.props.isLoading) } )}>
                <span className="filter-heading">Filter</span>
                {
                    this.state.filtersList.map((filter, index) => {
                        let currentSelectedId = this.props.activeFilters[filter.attr],
                            isFilterActive = !!currentSelectedId,
                            currentSelectedObj = filter.list.find(el => el._id === currentSelectedId),
                            currentSelectedTitle = currentSelectedObj ? currentSelectedObj.title : '';

                        return (
                        <label className="filter-wrapper" key={`${filter.title}_${index}`}>
                            <span className="filter-title">{filter.title}:</span>
                            <div className={classNames("filter-select-wrapper", { active: isFilterActive })}
                                onClick={(e) => { this.filterChanged({ target: { value: '-1' } }, filter.attr, index) }}
                            >
                                <span className="active-filter-label">
                                    {currentSelectedTitle}
                                    <div className="remove-filter close-icon"></div>
                                </span>
                                <select name={filter.attr} className="filter-select" 
                                    value={(this.state.currentFilters[filter.attr] | '-1')}
                                    onChange={(e) => { this.filterChanged(e, filter.attr, index) }}
                                >
                                    <option value="-1">...</option>
                                    {
                                        filter.list && filter.list.length ?
                                            filter.list.map( (filterEl, ind) => (
                                                <option value={ind} key={filterEl._id}>
                                                    { filterEl.title }
                                                </option>
                                            )) 
                                            : 
                                            ''
                                    }
                                </select>
                            </div>
                        </label>
                        )
                    })
                }
                {
                    this.props.search.length ? 
                    <div className="current-search-wrapper">
                        <span className="divider">|</span>
                        <label className="current-search-string">
                            {this.props.search}
                        </label>
                    </div>
                    :
                    ''
                }
            </div>
        );
    }
}

const mapStoreToProps = store => {
    let { all, filtered, filters, isLoading, search } = store.videosReducer;

    return {
        all,
        filtered,
        isLoading,
        activeFilters: filters,
        search
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        videosIsIsntLoading,
        fetchFilteredVideos,
        addFilters
    }, dispatch)
};

export default connect(mapStoreToProps, mapDispatchToProps)(Filters);