import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';

import PageHeading from '../PageHeading/PageHeading';
import VideosList from '../VideosList/VideosList';
import Footer from '../Footer/Footer';

import './home.scss';

class Home extends Component {
    render() {
        return (
            <>
                <main>
                    <section className="videos-block-wrapper">
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                    <PageHeading title={'Discover'} 
                                        description={'watch videos in full length, ad-free and completely free.'}
                                    />
                                </div>
                            </div>
                            <VideosList/>
                        </div>
                    </section>
                </main>
                <Footer/>
            </>
        );
    }
}

const mapStoreToProps = store => {
    let { all, filtered, filters } = store.videosReducer;

    return {
        all,
        filtered,
        filters
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
    }, dispatch)
};

export default connect(mapStoreToProps/*, mapDispatchToProps*/)(Home);