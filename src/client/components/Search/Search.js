import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import classNames from 'classnames';

import { fetchFilteredVideos, addSearch, videosIsIsntLoading } from './../../actions';
import { fetchVideosPromise } from './../../common/utilities/api';

import './search.scss';

class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchOpened: false,
            touched: false,
            searchString: ''
        };
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);

        if(this.inputRef) {
            this.inputRef.addEventListener('keypress', this.searchVideoWithEnter);
        }
    }
    
    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);

        if(this.inputRef) {
            this.inputRef.removeEventListener('keypress', this.searchVideoWithEnter);
        }
    }
    
    handleClickOutside = (event) => {
        if(this.state.searchOpened && !this.state.searchString) {
            if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
                this.setState({ searchOpened: false });
            }
        }
    }

    setWrapperRef = (node) => {
        this.wrapperRef = node;
    }

    setInputRef = (node) => {
        this.inputRef = node;
    }

    openSearch = () => {
        this.setState({ searchOpened: true });
    }

    changeSearchString = (e) => {
        let newState = {
            searchString: e.target.value
        };

        if(!this.state.touched) {
            newState.touched = true;
        }

        this.setState(newState);
    }

    redirectToHome = () => {
        this.props.history.push('/');
    }

    searchVideo = () => {
        let newFilters = { ...this.props.filters },
            oldSearch = this.props.search;

        if(this.props.history && this.props.history.location !== '/') {
            this.redirectToHome()
        }

        if(this.state.touched) {
            newFilters.search = this.state.searchString;
            this.props.addSearch(newFilters.search);
            this.setState({ searchString: '', searchOpened: false });
        
            this.props.videosIsIsntLoading(true);
            fetchVideosPromise(
                (res) => {
                    this.props.fetchFilteredVideos(res.data);
                },
                (err) => {
                    this.props.videosIsIsntLoading(true);
                    toast.error('Error!');
                    this.props.addSearch(oldSearch);
                }, 
                newFilters
            );
        }
    }

    searchVideoWithEnter = (e) => {
        if (e.which == 13) {
            this.searchVideo();
        }
    }

    render() {
        return (
            <div className="search-wrapper">
                <div className={classNames(
                    "search-input-wrapper",
                    { 'active': this.state.searchOpened }
                    )} 
                    ref={this.setWrapperRef}
                >
                    <input type="search" id="site-search" name="q" onChange={this.changeSearchString} value={this.state.searchString}
                        aria-label="Search through site content" placeholder="Search here..." ref={this.setInputRef}/>
                    <div className="search-icon" onClick={this.state.searchOpened ? this.searchVideo : this.openSearch}></div>
                </div>
            </div>
        );
    }
}

const mapStoreToProps = store => {
    let { all, filtered, filters, search } = store.videosReducer;

    return {
        all,
        filtered,
        filters,
        search
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        videosIsIsntLoading,
        fetchFilteredVideos,
        addSearch
    }, dispatch)
};

export default connect(mapStoreToProps, mapDispatchToProps)(withRouter(Search));