import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { MultiSelect } from 'react-selectize';
import { toast } from 'react-toastify';

import 'react-selectize/dist/index.min.css';

import { fetchFilteredVideos, addMultiSearch, videosIsIsntLoading } from './../../actions';
import { fetchVideosPromise } from './../../common/utilities/api';

import './multisearch.scss';

class Multisearch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchString: ''
        };
    }

    renderValue = ({ label, value }) => {
        return (
            <div className="simple-value">
                <span className="value-text">{label}</span>
                <div className="value-remove close-icon" onClick={() => { this.handleOnRemoveItemClick(value); }}></div>
            </div>
        );
    }

    handleOnRemoveItemClick(selectedValue) {
        let oldTags = this.props.tags,
            newTags = oldTags.filter(v => v.label !== selectedValue);

        this.searchVideo(newTags);
    }

    createFromSearch(options, values, search) {
        let labels = values.map(function(value){ 
            return value.label; 
        })
        if (search.trim().length == 0 || labels.indexOf(search.trim()) != -1) {
            return null;
        }
        return {label: search.trim(), value: search.trim()};
    }

    onValuesChange = (tags) => {
        let newItem = tags[tags.length - 1],
            isUnique =  !this.props.tags.find(opt => {
                if(newItem) {
                    return opt.label.toLowerCase() === newItem.label.toLowerCase();
                } else {
                    return true;
                }
            });

        if(isUnique) {
            this.searchVideo([ ...tags ]);
        }
    }

    setInputFocus = (e) => {
        if(this.multiSelect) {
            try {
                let input = this.multiSelect.refs.select.refs.search;
                if(!e.target.classList.contains('value-remove')) {
                    input.focus();
                }
            } catch(err) {
                console.log(err);
            }
        }
    }

    toggleInputFocus = () => {
        if(this.multiSelect) {
            try {
                let input = this.multiSelect.refs.select.refs.search;

                input.blur();
                input.focus();
            } catch(err) {
                console.log(err);
            }
        }
    }

    setMultiSelectRef = (node) => {
        this.multiSelect = node;
    }

    redirectToHome = () => {
        this.props.history.push('/');
    }

    searchVideo = (tags) => {
        let oldSearch = this.props.multiSearch.map((tag) => tag.label),
            currentArray = tags ? [ ...tags ] : [ ...this.props.tags ],
            newSearch = currentArray.map((tag) => tag.label),
            newFilters = { multiSearch: JSON.stringify(newSearch) };

        if(this.props.history && this.props.history.location.pathname !== '/') {
            this.redirectToHome();
        }

        this.props.addMultiSearch(newSearch);
        this.toggleInputFocus();
    
        this.props.videosIsIsntLoading(true);
        fetchVideosPromise(
            (res) => {
                this.props.fetchFilteredVideos(res.data);
            },
            (err) => {
                this.props.videosIsIsntLoading(true);
                toast.error('Error!');
                this.props.addMultiSearch(oldSearch);
            }, 
            newFilters
        );
    }

    render() {
        const { tags } = this.props;
        return (
            <div className="search-wrapper">
                <div className={classNames("search-input-wrapper")} 
                    ref={this.setWrapperRef}
                    onClick={this.setInputFocus}
                >
                    <MultiSelect
                        values={[ ...tags ]}
                        ref={this.setMultiSelectRef}
                        placeholder="Search here and press enter (return) key"
                        maxValues={8}
                        createFromSearch={this.createFromSearch}
                        onValuesChange={this.onValuesChange}
                        renderValue={this.renderValue}
                    />
                    <div className="search-icon"></div>
                </div>
            </div>
        );
    }
}

const mapStoreToProps = store => {
    let { all, filtered, filters, multiSearch } = store.videosReducer,
        tags = multiSearch.map((tag) => ({ label: tag, value: tag }));

    return {
        all,
        filtered,
        filters,
        multiSearch,
        tags
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        videosIsIsntLoading,
        fetchFilteredVideos,
        addMultiSearch
    }, dispatch)
};

export default connect(mapStoreToProps, mapDispatchToProps)(withRouter(Multisearch));