import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import { toast } from 'react-toastify';

import { fetchVideosPromise } from './../../common/utilities/api';
import { fetchAllVideos, videosIsIsntLoading } from './../../actions';

import './videosList.scss';

class VideosList extends Component {
    componentDidMount() {
        if(!this.props.videos.length) {        
            this.props.videosIsIsntLoading(true);
            fetchVideosPromise((res) => {
                if(res && res.data) {
                    this.props.fetchAllVideos([ ...res.data]);
                }
            }, (err) => {
                this.props.videosIsIsntLoading(false);
                toast.error('Error!');
            })
        }
    }

    render() {
        const videosSourse = (this.props.filters && (Object.keys(this.props.filters)).length) || this.props.search || this.props.multiSearch.length ? 
            [ ...this.props.filtered ] 
            : 
            [ ...this.props.videos ];

        return (
            <div className={classNames("videos-list-wrapper", { 'loading active': this.props.isLoading })}>
                <div className="row videos-row">
                    { videosSourse.length ?
                        videosSourse.map((video) => (
                            <article key={(video._id).toString()} className="video-col col-12 col-sm-6 col-md-4 col-lg-3">
                                <div className="video-wrapper">
                                    <Link to={`/videos/${video._id}`}>
                                        <div className="video-poster-wrapper" style={{ backgroundImage: (video.imageUrl ? `url(${video.imageUrl})` : undefined) }}>
                                            <div className="play-button"></div>
                                        </div>
                                        <div className="video-info">
                                            <h3 className="video-title">{video.title}</h3>
                                            <span className="video-description">{video.shortDescription}</span>
                                        </div>
                                    </Link>
                                </div>
                            </article>
                        ))
                        :
                        <article className="col-12 col-sm-6 col-md-4 no-videos-found">
                            <div className="no-videos-image"></div>
                            <div className="no-videos-text">
                                <h2>ooops, no videos found.</h2>
                                <div>please change the search filter.</div>
                            </div>
                        </article>
                    }
                </div>
            </div>
        );
    }
}

const mapStoreToProps = store => {
    let { all, filtered, filters, search, isLoading, multiSearch } = store.videosReducer;

    return {
        videos: [ ...all ],
        filtered: [ ...filtered ],
        filters: { ... filters },
        isLoading,
        search,
        multiSearch
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchAllVideos,
        videosIsIsntLoading
    }, dispatch)
};

export default connect(mapStoreToProps, mapDispatchToProps)(VideosList);