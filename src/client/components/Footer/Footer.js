import React, {Component} from "react";
import { compose, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';

import MultiSearch from './../MultiSearch/MultiSearch';

import './footer.scss';

class Footer extends Component {
    supportMail = 'support@checkass.com'

    state = {
        footerRef: null
    }

    componentDidMount() {
        this.setState({
            footerRef: this.footerRef
        })
    }

    setFooterRef = (node) => {
        this.footerRef = node;
    }

    render() {        
        let { isFull, onlyMobile } = this.props;

        return (
            <>
                <footer className={classNames({ "d-md-none": onlyMobile }, { "footer-full": isFull})}>
                    <div className="container">
                        <div className="row">
                            <div className={classNames("col-12", { "col-sm-6": !isFull })}>
                                <MultiSearch/>
                            </div>
                            <div className={classNames("col-12 col-sm-6", { "hidden": isFull })}>
                                <div className="support-mail-wrapper">
                                    <a href={`mailto:${this.supportMail}`} className="support-mail">support</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                <div className={classNames("footer-overlay", { "d-md-none": onlyMobile })}></div>
            </>
        );
    }
}

const mapStoreToProps = store => {
    let { all, filtered, filters } = store.videosReducer;

    return {
        all,
        filtered,
        filters
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
    }, dispatch)
};

export default connect(mapStoreToProps/*, mapDispatchToProps*/)(Footer);