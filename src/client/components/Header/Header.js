import React, {Component} from "react";
import { Link } from 'react-router-dom';
import { withRouter } from "react-router";

import Navigation from './../Navigation/Navigation';

import './header.scss';

class Header extends Component {
    state = {
        needsNavigation: false
    }

    componentDidMount() {
        if(this.props.location && this.props.location.pathname !== '/') {
            this.setState({
                needsNavigation: true
            });
        }
    }

    componentDidUpdate(prevProps) {
        if(this.props.location && prevProps.location) {
            if (this.props.location.pathname !== prevProps.location.pathname) {
                this.onRouteChanged(this.props.location);
            }
        }
    }

    onRouteChanged(location) {
        if(location.pathname !== '/') {
            this.setState({
                needsNavigation: true
            });
        } else {
            this.setState({
                needsNavigation: false
            });
        }
    }

    render() {
        return (
            <>
                <header>
                    <div className="container">
                        <div className="row">
                            <div className={ this.state.needsNavigation ? "col-12 col-sm-2" : "col-12" }>
                                <div className="logo-wrapper">
                                    <Link to="/">
                                        <h1 className="logo">
                                            Checkass
                                        </h1>
                                    </Link>
                                </div>
                            </div>
                            {
                                this.state.needsNavigation ?

                                <Navigation className="col-12 col-sm-10"/>
                                :
                                ''
                            }
                        </div>
                    </div>
                </header>
                <div className="header-overlay"></div>
            </>
        );
    }
}

export default withRouter(Header);