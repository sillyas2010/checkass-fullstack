const express = require('express'),
  { Video, Actor, Category } = require('../models/index'),
  srcDir = (__dirname).split('/src')[0],
  env = require('dotenv').config({ path: srcDir + '/.env' }),
  app = express(),
  router = express.Router();

router.get('/fetch-categories', (req, res)=>{
  Category.find({}, (err, result)=>{
    if(err){ 
      res.status(404).json(err)
    };
    res.status(200).json(result);
  })
})

router.get('/fetch-filters', async (req, res)=>{
  const category = await Category.find({}, (err, res)=>{
    if(err){
      res.status(404).json(err)
    }
  })
  const actor = await Actor.find({}, (err, res)=>{
    if(err){
      res.status(404).json(err)
    }
  })

  res.status(200).json([{ title: 'Category', attr: 'category', list: category }, { title: 'Actor', attr: 'actors', list: actor }])
})

router.get('/fetch-videos', (req, res)=>{
  let reqFilter = {};

  if(req.query && req.query.filters) {
    try {
      reqFilter = JSON.parse(req.query.filters);
    } catch(err) {
      console.log(err);
      res.status(400).json(err);
    }
  }

  const filters = {};
  if(reqFilter.multiSearch) {
    let searchArr = [];

    if(!Array.isArray(reqFilter.multiSearch)) {
      if(typeof reqFilter.multiSearch === 'string') {
        try {
          reqFilter.multiSearch = JSON.parse(reqFilter.multiSearch);
        } catch(err) {
          console.log(err);
          res.status(400).json(err);
        }
      }
    } 
      
    if(reqFilter.multiSearch.length) {
      filters.tags = { $exists: true };
      filters.$or = [];

      searchArr = reqFilter.multiSearch.forEach((tag) => { 
        filters.$or.push({
           "tags": new RegExp(`.*${tag}.*`, 'i')
        });
      });
    }

  } else {
    if(reqFilter.search){
      let regex = new RegExp(`.*${reqFilter.search}.*`);
      filters.title = { $regex: regex, $options: "i" };
    }
    if(reqFilter.category){
      filters["category._id"] = reqFilter.category;
    } 
    if(reqFilter.actors){
      filters.actors = { $elemMatch: { _id: reqFilter.actors } };
    }
  }

  Video.find(filters, (err, result)=>{
      if(err){ 
        res.status(404).json({ err: err });
      };
      res.status(200).json(result);
    })
})

router.get('/fetch-actors', (req, res) => {
  Actor.find({}, (err, result)=>{
    if(err){ 
      res.status(404).json({ err: err });
    };
    res.status(200).json(result);
  })
})

module.exports = router;