const express = require('express'),
      api = require('./api/index'),
      router = require('./routers/index')
      mongoose = require('mongoose'),
      fs = require('fs'),

      srcDir = (__dirname).split('/src')[0],
      env = require('dotenv').config({ path: srcDir + '/.env' });
      

const app = express();

const { DB_USERNAME, DB_PASSWORD } = process.env;


app.use(express.static('dist'));

app.use('/api', api);
app.use('/save', router);

app.get('/', (req, res) => {
    fs.readFile(__dirname + '/index.html', 'utf8', (err, text) => {
        res.send(text);
    });
});
const uri = `mongodb+srv://${DB_USERNAME}:${DB_PASSWORD}@cluster0-k1l6p.mongodb.net/test?retryWrites=true&w=majority`;

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
