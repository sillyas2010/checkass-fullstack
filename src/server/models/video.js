const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
      title: String,
      url: String,
      preview: String,
      shortDescription: String,
      fullDescription: String,
      actors: [ 
          { title: String, 
            _id: Schema.Types.ObjectID } 
        ],
      quality: String,
      imageUrl: String,
      publishDate: Date,
      category: { title: String,
      _id: Schema.Types.ObjectID },
      tags: [ String ]
    });

function returnTags(video) {
  let tags = [];

  if(video.title) {
    tags.push(video.title);
  }

  if(video.category && video.category.title) {
    tags.push(video.category.title);
  }

  if(video.actors && video.actors.length) {
    video.actors.forEach((actor) => {
      if(actor.title) {
        tags.push(actor.title);
      }
    })
  }

  return tags;
}

function onUpdate(next) {
  let needsTags = false;

  if(!this.title) {
    if(this._update) {
      if(this._update.$set) {
        if(!this._update.$set.tags && !this._update.$set.tags.length) {
          this._update.$set.tags = returnTags(this._update.$set);
        }
      }
    }

    next();
    return;
  }

  if(!this.tags || !this.tags.length) {
    needsTags = true;
  } else {
    let isModified = this.isModified('title') || this.isInit('title') ||
        this.isModified('actors') || this.isInit('actors') ||
        this.isModified('category') || this.isInit('category') || false;

    if(isModified) {
      needsTags = true;
    }
  }

  if(needsTags) {
    this.tags = returnTags(this);
  }

  next();
}

schema.pre('save', onUpdate);
schema.pre('update', onUpdate);
schema.pre('findOneAndUpdate', onUpdate);

module.exports = mongoose.model('video', schema);