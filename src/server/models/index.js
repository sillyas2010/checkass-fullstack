const Video = require('./video'),
      Category = require('./category'),
      Actor = require('./actor');

module.exports = { Video, Category, Actor };