const express = require('express'),
  { Video, Category, Actor } = require('../models/index'),
  AWS = require('aws-sdk'),
  mongoose = require('mongoose'),

  srcDir = (__dirname).split('/src')[0],
  env = require('dotenv').config({ path: srcDir + '/.env' }),
  app = express(),
  router = express.Router();

var credentials = new AWS.SharedIniFileCredentials({profile: 'wasabi'});
AWS.config.credentials = credentials;
var ep = new AWS.Endpoint('s3.wasabisys.com');
var s3 = new AWS.S3({
  endpoint: ep,
  region: 'eu-central-1',
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});


const listAllObjectsFromS3Bucket = async (bucket, prefix) =>{
  let isTruncated = true;
  let marker;
  const videos = [];

  while(isTruncated) {
    let params = { Bucket: bucket };
    if (prefix) params.Prefix = prefix;
    if (marker) params.Marker = marker;
    try {
      const response = await s3.listObjects(params).promise();

      for(item of response.Contents){
        const folder = item.Key.split("/")[0];

        if(item.Key.split("/")[1]){
          const name = item.Key.split("/")[1].split('.')[0];

          const videoData = folder === 'videos' ? {
              title: name,
              url: `${process.env.VIDEO_BUCKET_URL}${item.Key}`
            } : {
              title: name,
              imageUrl: `${process.env.VIDEO_BUCKET_URL}${item.Key}`
            };

          const video = await findAndUpdateOrCreate(Video, { title: name }, videoData);

          if(videos.findIndex(e => e.title === name) === -1){ 
            videos.push(video);
          }else{
            videos[videos.findIndex(e => e.title === name)] = video;
          }
        }
      }

      isTruncated = response.IsTruncated;
      if (isTruncated) {
        marker = response.Contents.slice(-1)[0].Key;
      }
      return videos;

    } catch(error) {
      throw error;
    }
  }
}

const manageVideoData = async (obj, callback) => {
  const category = await findAndUpdateOrCreate(Category, { title: obj.category }, { title: obj.category });

  const actors = []
  for(const item of obj.actors){
    const actor = await findAndUpdateOrCreate(Actor, { title: item }, { title: item })
    actors.push({ title: actor.title, _id: new mongoose.mongo.ObjectId(actor._id)})
  }

  const newItem = { ...obj,
    category: { title: category.title, 
      _id: new mongoose.mongo.ObjectId(category._id)}, 
    actors: actors };

  const newVideo = await Video.findOneAndUpdate({ title: newItem.title }, 
    { $set: { ...newItem } }, 
    { useFindAndModify: false, new: true });

  return newVideo;
}

const findAndUpdateOrCreate = async (model, identifyObj, objectVal) =>{

  const obj = await model.findOneAndUpdate(
    { ...identifyObj },
    { $set: { ...objectVal }},
    { useFindAndModify: false, new: true });

  if(!obj){
    const newObj = await model.create({ ...objectVal });
    return newObj;
  }else {
    return obj;
  }
}



router.get('/list-backet', async (req, res) => {
  const viedos = await listAllObjectsFromS3Bucket('testbaket');
  res.status(200).json(viedos);
})

router.get('/manage-backet', async (req, res) =>{
  const video = await Video.findOne({ title: 'video4' });
    const item = {
      title: video.title,
      shortDescription: 'Lorem ipsum dolor sit amet consectetur  elit',
      fullDescription: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint aspernatur unsit amet consecteturde magsit amet consecteturnam pariatur odit. Ducimus eos provident velit tempore eum! Earum atque illum mollitia voluptas reprehenderit corporis, ipsum minima enim?',
      actors: [ 'actor 1', 'actor 2' ],
      quality: 'some quality',
      publishDate: new Date(),
      category: 'New catigory 3'
    };

  manageVideoData(item)
    .then((result)=>{
      res.status(200).json(result);
    });
})

module.exports = router;